const matches = require('./matches.json');
// const deliveries = require('./deliveries.json');

var resultObj = {};

 matches.forEach((iterator) => {

    resultObj[iterator.season] = teamsWonList(iterator.season)

})

function teamsWonList(season) {

    let winningTeam = {};

    matches.forEach((iterator) => {

        if (iterator.season === season) {

            if (winningTeam[iterator.winner]){
                winningTeam[iterator.winner]++;
            }

            else{
                winningTeam[iterator.winner] = 1;
            }
            
        }

    })

    return winningTeam;

}
module.exports = resultObj;