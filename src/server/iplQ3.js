const matches = require('./matches.json');
const deliveries = require('./deliveries.json');



const idArr = matches.map((element) => {

    if (element.season === "2016") {
        return element.id;
    }

})

const newArr = idArr.filter((element) => {
    if (element != undefined) {
        return element;
    }
})


const result = deliveries.reduce((accumulator, currentValue) => {

    newArr.forEach((element) => {

        if (element == currentValue.match_id) {

            if (accumulator[currentValue.bowling_team]) {
                accumulator[currentValue.bowling_team] += parseInt(currentValue.extra_runs);
            }
            else {
                accumulator[currentValue.bowling_team] = parseInt(currentValue.extra_runs);
            }

        }

    })
    return accumulator;

}, {})

module.exports = result;
