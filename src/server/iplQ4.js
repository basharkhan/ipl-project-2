const matches = require('./matches.json');
const deliveries = require('./deliveries.json');

const arr = matches.map((element) => {

    if (element.season === '2015') {
        return element.id;
    }

})

// console.log(arr);

const idArr = arr.filter((element) => {
    if (element != undefined) {
        return element;
    }
})

// console.log(idArr);

const totalRuns = deliveries.reduce((accumulator, currentValue) => {


    idArr.forEach((element) => {

        if (element === currentValue.match_id) {
            if (accumulator[currentValue.bowler]) {
                accumulator[currentValue.bowler] += parseInt(currentValue.total_runs);
            }
            else {
                accumulator[currentValue.bowler] = parseInt(currentValue.total_runs);
            }
        }

    })

    return accumulator;

}, {})


// console.log(totalRuns);

// const totalBalls = deliveries.reduce((accumulator, currentValue) => {

//     idArr.forEach((element) => {

//         if (element === currentValue.match_id) {
//             if (accumulator[currentValue.bowler]) {
//                 accumulator[currentValue.bowler]++;
//             }
//             else{
//                 accumulator[currentValue.bowler] = 1;
//             }
//         }

//     })

//     return accumulator;

// }, {})

// console.log(totalBalls);



//  for( const property in totalBalls ) {
//      totalBalls[property] = totalBalls[property]/6;
//  }

 for( const property in totalRuns ){
     totalRuns[property] = totalRuns[property]/totalBallsFn(property);  
 }

 function totalBallsFn(property){


    var overs = 0;
    deliveries.forEach( (delElement) => {

        idArr.forEach( (idElement) => {
            if( idElement === delElement.match_id ){
                if(delElement.bowler == property){
                    overs++;
                }
            }
        } )
        
    } )

    
    return overs/6;
    
 }

 var sortable = [];
for (var property in totalRuns) {
    sortable.push([property, totalRuns[property]]);
}

sortable.sort(function(a, b) {
    return a[1] - b[1];
});

var tenEconomy = [];
for(let i=0; i<10; i++){
    tenEconomy[i] = sortable[i];
}

var objSorted = {}
tenEconomy.forEach(function(element){
    objSorted[element[0]]=element[1]
})



module.exports = objSorted;
 