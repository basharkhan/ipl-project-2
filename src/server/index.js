//  1. Matches won per year for all years

let matchesPerYear = require('./iplQ1.js');

console.log(matchesPerYear);

let matchesPerYearInJson = JSON.stringify(matchesPerYear);

console.log(matchesPerYearInJson);

// 2. Matches won per team per year

const newObj = require('./iplQ2.js');

console.log(newObj);

// 3. Extra runs given by each bowling team in 2016

const extraRuns = require('./iplQ3');
console.log(extraRuns);

const extraRunsInJson = JSON.stringify(extraRuns);
console.log(extraRunsInJson);


// 4. Top ten Economy bowlers
const objSorted = require('./iplQ4');
console.log(objSorted); 

const topTenInJson = JSON.stringify(objSorted);
console.log(topTenInJson);


